<?php

namespace Lpp\Entity;

use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validation;

/**
 * Represents a single item from a search result.
 *
 */
class Item {
    /**
     * Name of the item
     *
     * @var string
     */
    public $name;

    /**
     * Url of the item's page
     *
     * @var string
     */
    public $url;

    /**
     * Unsorted list of prices received from the
     * actual search query.
     *
     * @var Price[]
     */
    public $prices = [];

    public function validateUrl($url): bool {
        $validator = Validation::createValidator();
        $violations = $validator->validate($url, [
            new Url(),
        ]);
        return count($violations) === 0;
    }
}
