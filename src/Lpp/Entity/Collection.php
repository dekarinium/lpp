<?php
namespace Lpp\Entity;

/**
 * Represents a single brand in the result.
 *
 */
class Collection
{
    /**
     * Name of the brand
     *
     * @var string
     */
    public $collection;

    /**
     * Brand's description
     * 
     * @var string
     */
    public $id;

    /**
     * Unsorted list of items with their corresponding prices.
     * 
     * @var Item[]
     */
    public $brands = [];
}
