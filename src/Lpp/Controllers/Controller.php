<?php

namespace Lpp\Controllers;

use Noodlehaus\Config;

class Controller {
    function render($name, $context = []) {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . $this->getConfig("views"));
        $twig = new \Twig_Environment($loader, array(
            'cache' => __DIR__ . $this->getConfig("cache"),
            'auto_reload' => true
        ));
        return $twig->render($name, $context);
    }

    function getConfig($name) {
        $conf = Config::load('config/config.json');
        return $conf->get($name);
    }
}