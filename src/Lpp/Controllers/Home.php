<?php

namespace Lpp\Controllers;

use Lpp\Service\ItemService;
use Lpp\Service\OrderedBrandService;
use Lpp\Controllers\Controller;

class Home extends Controller {
    public function index() {
        $itemService = new ItemService();
        $brandService = new OrderedBrandService($itemService);
        $brands = $brandService->getBrandsForCollection('winter');
        $items = $brandService->getSortedItemsForCollection('winter');
        echo $this->render("index.html", [
            'items' => $items,
            'brands' => $brands
        ]);
    }
}