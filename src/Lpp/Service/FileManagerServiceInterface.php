<?php

namespace Lpp\Service;

interface FileManagerServiceInterface {
    /**
     * @param
     *
     * @return object
     */
    public function getData();
}