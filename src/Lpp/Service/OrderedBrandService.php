<?php

namespace Lpp\Service;

class OrderedBrandService extends UnorderedBrandService {
    /**
     * @param string $collectionName Name of a collection to search for
     *
     * @return \Lpp\Entity\Item[]
     */
    public function getSortedItemsForCollection($collectionName): array {
        $items = parent::getItemsForCollection($collectionName);
        usort($items, function ($a, $b) {
            return $a->name < $b->name;
        });
        return $items;
    }
}