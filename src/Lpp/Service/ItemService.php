<?php

namespace Lpp\Service;

use Lpp\Entity\Brand;
use Lpp\Entity\Item;
use Lpp\Entity\Price;
use Lpp\Entity\Collection;
use Lpp\Service\FileManagerService;
use Lpp\Service\ItemServiceInterface;
use \Niko9911\JsonToEntity\Mapper;
use phpDocumentor\Reflection\Types\Object_;

class ItemService implements ItemServiceInterface {
    private $mapper;

    public function __construct() {
        $this->mapper = new Mapper();
    }

    /**
     * This method should read from a datasource (JSON for case study)
     * and should return an unsorted list of brands found in the datasource.
     *
     * @param int $collectionId
     *
     * @return Lpp\Entity\Brand[]
     */
    public function getResultForCollectionId($collectionId): array {
        $collections = $this->getData();
        if (is_array($collections)) {
            foreach ($collections as $collection) {
                $collectionsObject = $this->mapper->map($collection, Collection::class);
                if ($collectionsObject->id == $collectionId) {
                    return $this->mapBrands($collectionsObject->brands);
                }
            }
        } else {
            return $this->mapBrands($collections->brands);
        }
        return [];
    }

    function getData() {
        $fileManagerService = new FileManagerService();
        return $fileManagerService->getData();
    }

    private
    function mapBrands($inputBrands): array {
        $brands = [];
        if (!empty($inputBrands)) {
            foreach ($inputBrands as $brand) {
                $brand = $this->mapper->map($brand, Brand::class);
                $brand->items = $this->mapItems($brand->items);
                $brands[] = $brand;
            }
        }
        return $brands;
    }

    private
    function mapItems($inputItems): array {
        $items = [];
        if (!empty($inputItems)) {
            foreach ($inputItems as $item) {
                if (Item::validateUrl($item->url)) {
                    $item = $this->mapper->map($item, Item::class);
                    $item->prices = $this->mapPrices($item->prices);
                    $items[] = $item;
                }
            }
        }
        return $items;
    }

    private
    function mapPrices($inputPrices): array {
        $prices = [];
        if (!empty($inputPrices)) {
            foreach ($inputPrices as $price) {
                $newPrice = new Price();
                $newPrice->arrivalDate = $price->arrival;
                $newPrice->description = $price->description;
                $newPrice->dueDate = $price->due;
                $newPrice->priceInEuro = $price->priceInEuro;
                $prices[] = $newPrice;
            }
        }
        return $prices;
    }
}