<?php

namespace Lpp\Service;

use Noodlehaus\Config;

class FileManagerService implements FileManagerServiceInterface {

    public function getData() {
        $conf = Config::load('config/config.json');
        $path = __DIR__ . $conf->get('json_path');
        if (file_exists($path)) {
            return json_decode(file_get_contents($path));
        }
        throw new \Exception(sprintf('File: %s not found', $path));
    }
}