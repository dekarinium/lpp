<?php

use Lpp\Service\ItemService;
use \Lpp\Service\UnorderedBrandService;
use Lpp\Entity\Brand;
use \PHPUnit\Framework\TestCase;

class UnorderedBrandServiceTest extends TestCase {
    public function testGetBrandsForCollection() {
        $collectionName = 'winter';
        $brand = new Brand();
        $brand->description = "description txt";
        $brand->name = "brand name";


        $itemService = $this->createMock(ItemService::class);
        $itemService
            ->expects($this->any())
            ->method('getResultForCollectionId')
            ->willReturn([$brand, new Brand()]);

        $unorderedBrandService = new UnorderedBrandService($itemService);
        $items = $unorderedBrandService->getBrandsForCollection($collectionName);
        $this->assertEquals(count($items), 2);
        $this->assertEquals($items[0]->name, "brand name");
        $this->assertEquals($items[0]->description, "description txt");
        $this->assertEquals($items[1]->description, null);
    }
}