<?php

use \Lpp\Service\ItemService;
use \PHPUnit\Framework\TestCase;

class ItemServiceTest extends TestCase {
    public function testGetResultForCollectionId() {
        $collectionId = 1315475;
        $jsonData = '{
  "collection": "winter",
  "id": 1315475,
  "brands": {
    "1": {
      "name": "XXX",
      "description": "New winter collection",
      "items": {
        "1000": {
          "name": "jacket",
          "url": "http://www.example.com",
          "prices": {
            "1001": {
              "description": "Initial price",
              "priceInEuro": 108,
              "arrival": "2017-01-03",
              "due": "2017-01-20"
            }
          }
        }
      }
    }
  }
}';

        $itemService = $this->getMockBuilder(ItemService::class)->setMethods(['getData'])->getMock();
        $itemService->expects($this->once())->method('getData')->will($this->returnValue(json_decode($jsonData)));
        $items = $itemService->getResultForCollectionId($collectionId);
        $this->assertEquals($items[0]->name, "XXX");
        $this->assertEquals(count($items[0]->items), 1);
        $this->assertEquals(count($items[0]->items[0]->prices), 1);
        $this->assertEquals($items[0]->items[0]->prices[0]->priceInEuro, 108);
    }
}